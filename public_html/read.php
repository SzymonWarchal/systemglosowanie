<?php

/**
 * Function to query information based on 
 * a parameter: in this case, location.
 *
 */

if (isset($_POST['submit'])) {
    try  {
        require "../../../config.php";
        require "../../../common.php";
        $dbname     = "projekti_user"; 
        $dsn  = "mysql:host=$host;dbname=$dbname";

        $connection = new PDO($dsn, $username, $password, $options);
        $location = $_POST['kod'];
        $sql = "SELECT *
                        FROM `odpowiedz`
                        WHERE kod = $location";

        
        $statement = $connection->prepare($sql);
        $statement->bindParam(':location', $location, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();


   
    } catch(PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
}
?>
<?php require "templates/header.php"; ?>
 <br>       
<?php  
if (isset($_POST['submit'])) {
    if ($result && $statement->rowCount() > 0) { ?>
<h2><b>Twoja odpowiedź na pytanie:</b></h2>

        <?php foreach ($result as $row) { ?>
            
            <?php $checksum = escape($row["checksum"])?>
<?php $y=escape($row["kod"]); ?>
<?php $x=escape($row["answer"]); ?>
<?php $xx=escape($row["answer2"]); ?>
<?php $xxx=escape($row["answer3"]); ?>
<?php $z=$_POST['pesel']; ?>
           <?php $new = crc32("$x,$xx,$xxx,$y,$z"); ?>
           <?php if ($new != $checksum){ ?>

<div style="margin: 0px auto; background-color: #f50505; width: 50%; padding-top: 10px; padding-bottom: 10px;margin-bottom:15px;"> 
<?php echo escape($row["answer"]); ?></br><?php echo "Twoja odpowiedz została zmieniona!"; ?> </div>
<?php } else {?>
<div style="margin: 0px auto; background-color: #5eff89; width: 50%; padding-top: 10px; padding-bottom: 10px;margin-bottom:15px;"> 
<?php echo escape($row["answer"]); ?> <br>
<?php echo escape($row["answer2"]); ?> <br>
<?php echo escape($row["answer3"]); ?> </div>
<?php } ?>
<?php } ?>
    <?php } else { ?>
        <blockquote><div style="margin: 0px auto; background-color: #fc4747; width: 52%; padding-top: 10px; padding-bottom: 10px; margin-top: -20px; margin-bottom:15px;">Brak odpowiedzi w systemie dla kodu : <?php echo escape($_POST['kod']); ?>. </div></blockquote>
    <?php } 
} ?> 
<div style="margin: 0px auto; background-color: #f6f6f6; width: 50%; padding-top: 10px; padding-bottom: 10px;">

<h5>Sprawdź swoją odpowiedź dzięki <br> 
wcześniej wygenerowanemu kluczowi</h5>

<form method="post">
    <label for="kod" style=" font-size: 10px; padding-bottom: 5px;">Kod prywatności</label>
    <input type="text" id="kod" name="kod" style="text-align: center; width: 294px; padding-top: 10px; padding-bottom: 10px;">
<br>
<label for="kod" style=" font-size: 10px; padding-bottom: 5px;">Pesel</label>
    <input type="text" id="pesel" name="pesel" style="text-align: center; width: 294px; padding-top: 10px; padding-bottom: 10px;">
<br>
    <input type="submit" name="submit" value="Sprawdź odpowiedź" style="text-align: center; width: 300px; height: 40px; margin-top: 10px;">
</form>
<br>
</div>
<br>
<form>
<input type="button" style="width: 200px; height: 40px;" value="Wróc na stronę główną" onclick="window.location.href='index.php'" />
</form>
<br>

<?php require "templates/footer.php"; ?>

