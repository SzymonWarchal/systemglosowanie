<!DOCTYPE html>
<html lang="en">


<head>
<style>
    * {
      font-family: sans-serif;
    }
</style>  
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Ankieta dla studentów</title>

    <link rel="stylesheet" href="css/style.css" />
  </head>

  <body style="text-align:center;">

<img src="logo.png" alt="Logo Uniwersytetu Pedagogicznego" width="400" height="400">

<div style="margin:0px auto; background-color: #f6f6f6; width: 50%; padding-top: 30px; padding-bottom: 60px;">
    <h1>Ankieta dla studentów UP</h1>
  <h2>Ważne:</h2>
Wygeneruj swój unikatowy kod który zapewni poufność Twojemu wyborowi. 
<br>Podaj kod podczas wysyłania odpowiedzi, dzięki temu zawsze będziesz w stanie sprawdzić swój wybór.
<br>Pamiętaj, aby zapisać swój kod gdyż nie będzie możliwości jego odzyskania.
<br>
</div>

