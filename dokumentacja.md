# Konfiguracja połączenia - config.php #

W pliku **config.php** zapisane są konfigurację połączenia do bazy danych. 
Zapisujemy w nim dane do logowania oraz nazwę bazy danych do której chcemy się dostać. 
W przypadku wykorzystywania skryptu konieczna jest zmiana danych na odpowiednie dla własnego serwera.



```php
<?php

/**
  * Configuration for database connection
  *
  */

$host       = "localhost";
$username   = "projekti";
$password   = "2M0evwb3F0";

$dbname   = "projekti_admin";
$dsn    = "mysql:host=$host;dbname=$dbname";

$options    = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
              );
```



# Połączenie z bazą danych - connect.php #

W pliku **connect.php** otwieramy połączenie do naszej bazy danych która to zadeklarowana 
została przez nas w pliku **config.php**.



```php
<?php

require "config.php";

try {
  $connection = new PDO("mysql:host=$host", $username, $password, $options);
  $sql = file_get_contents("data/init.sql");
  $connection->exec($sql);

  echo "Database and table users created successfully.";
} catch(PDOException $error) {
  echo $sql . "<br>" . $error->getMessage();
}
```



# Obróbka kodu HTML - common.php #

Plik odpowiadający za stworzenie funkcji do obróbki kodu HTML.



```php 
<?php

function escape($html) {
    return htmlspecialchars($html, ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8");
}
```



# Stworzenie nagłówka strony - public_html/templates/Header.php #

Dzięki wykorzystaniu templateów oszczędziliśmy sobie dorzucania dużej ilości kodu na każdej podstronie. 
Stworzyliśmy nagłówek strony w którym zawarte są najważniejsze informację które mają zostać wyswietlane 
w sekcji **Head**. Przede wszystkim zawarte w nim są wszelkie informację na temat meta danych, krótka 
informacja do użytkownika oraz logo uczelni. Wszystkie boxy które tam się znajdują posiadają 
zagnieżdżony styl CSS który odnosi się bezpośrednio do danej sekcji, oraz z góry narzucony 
font dla całej strony.



```php 
<!DOCTYPE html>
<html lang="en">


<head>
<style>
    * {
      font-family: sans-serif;
    }
</style>  
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Ankieta dla studentów</title>

    <link rel="stylesheet" href="css/style.css" />
  </head>

  <body style="text-align:center;">

<img src="logo.png" alt="Logo Uniwersytetu Pedagogicznego" width="400" height="400">

<div style="margin:0px auto; background-color: #f6f6f6; width: 50%; padding-top: 30px; padding-bottom: 60px;">
    <h1>Ankieta dla studentów UP</h1>
  <h2>Ważne:</h2>
Wygeneruj swój unikatowy kod który zapewni poufność Twojemu wyborowi. 
<br>Podaj kod podczas wysyłania odpowiedzi, dzięki temu zawsze będziesz w stanie sprawdzić swój wybór.
<br>Pamiętaj, aby zapisać swój kod gdyż nie będzie możliwości jego odzyskania.
<br>
</div>
```



# Stopka strony - public_html/templates/Footer.php #

Po stworzeniu nagłówka z całym szkieletem HTML potrzebne było stworzenie Stopki która będzie zamykała sekcje **body** i **html**.



```php 
</body>
</html>
```



# Strona główna - public_html/index.php #

W tym pliku znajduję się główna część strony. Importujemy do niego pliki **header.php** oraz **footer.php**. 
Dodatkowo pod nagłówkiem tworzymy box z przyciskami przekierowującymi do najważniejszych elementów strony 
czyli odpowiedzenia na ankiete oraz sprawdzenia swojej odpowiedzi.



```php 
<?php include "templates/header.php"; ?>

<div style="margin:0px auto;margin-top:10px; width: 50%; padding-top:10px; padding-bottom: 10px; background-color:#f6f6f6">
<br>
<form>
<input type="button" style="width: 300px; height: 40px;" value="Odpowiedz na ankietę" onclick="window.location.href='create.php'" />
</form>
<br>
<form>
<input type="button" style="width: 300px; height: 40px;" value="Sprawdź swoją odpowiedź" onclick="window.location.href='read.php'" />
</form>
<br>
</div>

    <?php include "templates/footer.php"; ?>
```



# Zapisywanie głosów - public_html/create.php #

Plik zawiera skrypt obsługujący zapisywanie danych do bazy. 



```php 
<?php

$error="n";

if (isset($_POST['submit'])) {
    require "../../../config.php";
    require "../../../common.php";

    try  {
        $connection = new PDO($dsn, $username, $password, $options);   

      $new_user = array(
            "answer"  => $_POST['answer']
        );

        $sql = sprintf(
                "INSERT INTO %s (%s) VALUES (%s)",
                "wyniki",
                implode(", ", array_keys($new_user)),
                ":" . implode(", :", array_keys($new_user))
        );
```



W pierwszej części importowane są wszystkie potrzebne pliki, tworzone jest nowe połączenie do bazy
oraz tworzona jest tablica danych, którą to przekażemy do bazy "wyniki" które w tym wypadku będą tylko odpowiedziami. 



```php 
        $statement = $connection->prepare($sql);
        $statement->execute($new_user);

        $dbname      = "projekti_studenci";
        $dsn    = "mysql:host=$host;dbname=$dbname";

     $connection = new PDO($dsn, $username, $password, $options);
        
        $new_user = array(
            "nr_indeksu" => $_POST['nr_indeksu'],
        );

        $sql = sprintf(
                "INSERT INTO %s (%s) VALUES (%s)",
                "studenci",
                implode(", ", array_keys($new_user)),
                ":" . implode(", :", array_keys($new_user))
        );
```



Następnie wysyłane jest przygotowane polecenie do bazy danych po którym to tworzone są nowe zmienne przechowujące nazwę bazy.
Analogicznie jak wyżej zapisywane są dane do tablicy ale w tym przypadku numery indexów trafiają do bazy studencji i wysyłane są do tablicy.



```php
       $statement = $connection->prepare($sql);
        $statement->execute($new_user);

        $dbname     = "projekti_user"; 
        $dsn   = "mysql:host=$host;dbname=$dbname";
        $connection = new PDO($dsn, $username, $password, $options);
        $x=$_POST['answer'];
        $y=$_POST['kod'];
        $checksum = crc32("$x,$y");
        printf($checksum);


        $new_user = array(
            "kod" => $_POST['kod'],
            "answer" => $_POST['answer'],
            "checksum" => $checksum
        );

        $sql = sprintf(
                "INSERT INTO %s (%s) VALUES (%s)",
                "odpowiedz",
                implode(", ", array_keys($new_user)),
                ":" . implode(", :", array_keys($new_user))
        );
        
        $statement = $connection->prepare($sql);
        $statement->execute($new_user);
        $error="nie";

    } catch(PDOException $error) {
        $error="tak";
    }
}
?>
```



Tworzona jest ostatnia już tablica z wygenerowanym kodem użytkownika, odpowiedzią i check sumą tych obu danych.
Dzięki takiemu rozwiązaniu możliwe będzie późniejsze potwierdzenie czy nasza odpowiedź zostałą zmieniona czy też nie. 
Gdyż w momencie zmiany przez admina czegokolwiek w naszym rekordzie, cała check suma się zmieni, a my otrzymamy komunikat o jej niezgodności.



```php
<?php require "templates/header.php"; ?>
<?php   if ($error=="tak"){  ?>
  <br><div  style="margin: 0px auto; background-color: #ff4c38; width: 50%; padding-top: 10px; padding-bottom: 10px;"><b>Już odpowiadałeś na tą ankietę.</b></div>
<?php }elseif (isset($_POST['submit']) && $statement) { ?>
       <br><div  style="margin: 0px auto; background-color: #5eff89; width: 50%; padding-top: 10px; padding-bottom: 10px;"><b>Odpowiedź została zapisana poprawnie.</b></div>
<?php }?>
<br>
<div style="margin: 0px auto; background-color: #f6f6f6; width: 50%; padding-top: 10px; padding-bottom: 10px;">
<?php $randomowa=rand(); ?>
<h4> Twój wygenerowany kod: <?php echo $randomowa; ?> </h4>
<h5>Zapisz go, jeśli chciałbyś kiedyś sprawdzić swoją odpowiedź. Przypomnienie go nie jest możliwe.</h5>
<h2>Uzupełnij poniższe pola:</h2>

<form method="post">
 <label for="kod" style=" font-size: 10px; padding-bottom: 5px;">Wygenerowany kod</label>
    <input type="text" style="text-align: center; width: 290px; padding-top: 20px; padding-bottom: 20px; "name="kod" id="kod" value="<?php echo $randomowa; ?>">
<br>
    <label for="nr_indeksu" style=" font-size: 10px; padding-bottom: 5px; margin-top: 20px;">Nr albumu studenta</label>
    <input type="text" style="text-align: center; width: 290px; padding-top: 20px; padding-bottom: 20px; "name="nr_indeksu" id="nr_indeksu">
<br>
    <label for="answer" style=" font-size: 10px; padding-bottom: 5px; margin-top: 20px;">Czy wiedza na warsztatach z programowania obiektowego była dla Ciebie przydatna?<br> Ogranicz się do odpowiedzi <b>Tak</b> lub <b>Nie</b>.</label>
    <input type="text" style="text-align: center; width: 290px; padding-top: 20px; padding-bottom: 20px; " name="answer" id="answer">
<br>
    <input type="submit" style="text-align: center; width: 300px; height: 40px; margin-top: 20px;" name="submit" value="Wyślij">
</form>
<br>
</div>
<br>
<form>
<input type="button" style="width: 200px; height: 40px;" value="Wróc na stronę główną" onclick="window.location.href='index.php'" />
</form>
<br>

<?php require "templates/footer.php"; ?>
```



W dalszej części kodu przechodzimy już do pracy nad wyglądem strony i stworzeniem odpowiednich rubryk, i przycisków które ułatwią użytkownikowi posługiwanie się ankietą.
W tej części również zawarte jest pytanie które wyświetla się w naszym formularzu które w tej wersji aplikacji jest ustalane na etapie tworzenia podstron. Tak jak w przypadku podstrony **index.php** importujemy tutaj pliki **header.php** oraz **footer.php** 
oszczędzając sobie przy tym pracę i ilość kodu na podstronie, a w reszcie podstrony tworzymy boxy i buttony które wystylizowane są bezpośrednio przy ich wywołaniu.

## Sprawdzanie odpowiedzi przez admina - public_html/tabelawynikow.php

W tym pliku stworzona jest tablica wyciągająca odpowiedzi z bazy danych i prezentująca je wszystkie na podstronie. Za całość odpowiada kod poniżej.

```php
<div style="text-align:center; margin:0px auto;margin-top:10px; width: 50%; padding-top:10px; padding-bottom: 10px; background-color:#f6f6f6">
<?php
$servername = "localhost";
$username = "projekti";
$password = "2M0evwb3F0";
$dbname = "projekti_user";
$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT answer, answer2, answer3 FROM odpowiedz";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  echo "<table><tr><th>Pytanie 1</th>  <th>Pytanie 2</th> <th>Pytanie 3</th></tr>";

  while($row = $result->fetch_assoc()) {
    echo "<tr><td>".$row["answer"]."</td><td>".$row["answer2"]."</td><td> ".$row["answer3"]."</td></tr>";
  }
  echo "</table>";
} else {
  echo "0 wyników";
echo "<tr><td>".$row["answer"]."</td><td>".$row["answer2"]."</td><td> ".$row["answer3"]."</td></tr>";
}
$conn->close();
?>
</div>
<br>
<form>
<input type="button" style="width: 200px; height: 40px;" value="Wróc na stronę główną" onclick="window.location.href='index.php'" />
</form>
<br>
```

Tabela znów jest wystylizowana stylami zagnieżdżonymi w headzie podstrony.

```css
table {
 
margin-left:auto; 
margin-right:auto;" 
margin-bottom: 20 px;
        }

th{
margin-right:10px;
padding-left:30px;
padding-right:30px;
border:2px solid;
text-align: center;
}

td{
padding-top: 5px;
padding-bottom: 5px;
border-bottom: 1px dotted;
text-align: center;
}
```

## Sprawdzanie odpowiedzi - public_html/read.php #

Plikiem który odpowiedzialny jest za sprawdzanie odpowiedzi w bazie danych przez użytkownika jest właśnie plik **read.php**.



```php
<?php

if (isset($_POST['submit'])) {
    try  {
        require "../../../config.php";
        require "../../../common.php";
        $dbname     = "projekti_user"; 
        $dsn  = "mysql:host=$host;dbname=$dbname";

        $connection = new PDO($dsn, $username, $password, $options);
        $location = $_POST['kod'];
        $sql = "SELECT *
                        FROM `odpowiedz`
                        WHERE kod = $location";

        
        $statement = $connection->prepare($sql);
        $statement->bindParam(':location', $location, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();
           
           } catch(PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
}
?>
```



W tym fragmencie importujemy potrzebne do wykonania tego zadania pliki, 
łączymy się z bazą danych w celu pobrania rekordów, 
szukamy w tabeli kodu podanego przez użytkownika i odpowiedzi przy nim widniejącej.



```php
<?php  
if (isset($_POST['submit'])) {
    if ($result && $statement->rowCount() > 0) { ?>
<h2><b>Twoja odpowiedź na pytanie:</b></h2>

        <?php foreach ($result as $row) { ?>
            
            <?php $checksum = escape($row["checksum"])?>
<?php $y=escape($row["kod"]); ?>
<?php $x=escape($row["answer"]); ?>
           <?php $new = crc32("$x,$y"); ?>
           <?php if ($new != $checksum){ ?>

<div style="margin: 0px auto; background-color: #f50505; width: 50%; padding-top: 10px; padding-bottom: 10px;margin-bottom:15px;"> 
<?php echo escape($row["answer"]); ?></br><?php echo "Twoja odpowiedz została zmieniona!"; ?> </div>
<?php } else {?>
<div style="margin: 0px auto; background-color: #5eff89; width: 50%; padding-top: 10px; padding-bottom: 10px;margin-bottom:15px;"> 
        <?php echo escape($row["answer"]); ?> </div>
<?php } ?>
<?php } ?>
    <?php } else { ?>
        <blockquote><div style="margin: 0px auto; background-color: #fc4747; width: 52%; padding-top: 10px; padding-bottom: 10px; margin-top: -20px; margin-bottom:15px;">Brak odpowiedzi w systemie dla kodu : <?php echo escape($_POST['kod']); ?>. </div></blockquote>
    <?php } 
} ?> 
```



Dany fragment odpowiedzialny już jest za wyświetlanie samej odpowiedzi. Sprawdza on czy check suma aktualnej odpowiedzi i kodu użytkownika, zgodna jest z tą która została wygenerowana podczas składania odpowiedzi.
W momencie jeśli system zauważy niezgodność check sumy wyświetla czerwony box z aktualną odpowiedzią i informacją, że odpowiedź użytkownika została zmieniona, jeśli jednak
suma jest zgodna z tą pierwotną, wyświetlana jest zielony box z odpowiedzią użytkownika.



```php
<?php require "templates/header.php"; ?>
 <br>       
...
<div style="margin: 0px auto; background-color: #f6f6f6; width: 50%; padding-top: 10px; padding-bottom: 10px;">

<h5>Sprawdź swoją odpowiedź dzięki <br> 
wcześniej wygenerowanemu kluczowi</h5>

<form method="post">
    <label for="kod" style=" font-size: 10px; padding-bottom: 5px;">Kod prywatności</label>
    <input type="text" id="kod" name="kod" style="text-align: center; width: 294px; padding-top: 10px; padding-bottom: 10px;">
<br>
    <input type="submit" name="submit" value="Sprawdź odpowiedź" style="text-align: center; width: 300px; height: 40px; margin-top: 10px;">
</form>
<br>
</div>
<br>
<form>
<input type="button" style="width: 200px; height: 40px;" value="Wróc na stronę główną" onclick="window.location.href='index.php'" />
</form>
<br>

<?php require "templates/footer.php"; ?>
```



W dalszej części kodu mamy tak jak w obu poprzednich podstronach wywołanie templateów **header.php** oraz **footer.php**.
Poza wywołaniami znajdują się tam wymagane boxy wraz z instrukcjami dla użytkownika i wymaganymi przyciskami do poprawnej interakcji z aplikacją.

## Stworzenie tabeli ##

W projekcie znajdują się trzy bazy, jednakże z bazy admin można spokojnie zrezygnować i obsłużyć program tylko na dwóch bazach. Konieczne wtedy jednak będzie pozbycie się odwołania do tabeli wyniki na podstronie **create.php**.

```sql
CREATE TABLE `wyniki` (
 `answer` varchar(11) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
```

Kod do wygenerowania tabeli Wyniki znajdującej się w bazie admin (nie jest wymagana)

```sql
CREATE TABLE `odpowiedz` (
 `kod` int(11) NOT NULL,
 `answer` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
 `checksum` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
 UNIQUE KEY `kod` (`kod`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
```

Kod do wygenerowania tabeli Odpowiedz znajdującej się w bazie user

```mysql
CREATE TABLE `studenci` (
 `nr_indeksu` int(11) NOT NULL,
 UNIQUE KEY `nr_indeksu` (`nr_indeksu`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
```

Kod do wygenerowania tabeli Studenci znajdującej się w bazie studenci

# W razie problemów kontakt podany w pliku README.md" #
