# System do anonimowego głosowania #

Prosty system oparty na bazach danych dzięki któremu możliwe jest oddawanie głosów w wcześniej przygotowanej ankiecie w taki sposób aby nie były one możliwe do powiązania z odpowiadającym, oraz aby nie było możliwości ich zmiany przez admina w taki sposób, że odpowiadający się o tym nie dowie.

Adres strony internetowej z uruchomionym skryptem aplikacji:
<http://projekt-inz-mw-sw.vxm.pl>

### Wymagania ###

* Bazy danych MySQL
* Platforma z możliwością obsługi PHP

### Jak uruchomić? ###

* Całe repozytorium należy pobrać i rozpakować
* Wrzucić zawartość na serwer lub odpalić zdalnie na swojej maszynie
* Przygotować dwie bazy danych w oparciu o kody podane w pliku dokumentacja
* Uruchomić stronę index.php

### Twórcy ###

* Wójcik Magdalena <magdalena.wojcik7@student.up.krakow.pl>
* Warchał Szymon <szymon.warchal1@student.up.krakow.pl>


### Dokumentacja ###

* Pełna dokumentacja projektu wraz z opisanymi wszystkimi skryptami znajduję się w pliku [dokumentacja.md](https://bitbucket.org/SzymonWarchal/systemglosowanie/src/master/dokumentacja.md)